# kc8vm version
VERSION = 1.0

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# includes and libs
INCS = -I. -I/usr/include
LIBS = -L/usr/lib -lc

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\" -D_GNU_SOURCE
#CFLAGS = -std=c99 -Wall -Wextra -pedantic -O0 -g ${INCS} ${CPPFLAGS}
CFLAGS = -std=c99 -Wall -Wextra -pedantic -Os ${INCS} ${CPPFLAGS}
# Solaris
#CFLAGS = -fast ${INCS} -DVERSION=\"${VERSION}\"
#LDFLAGS = ${LIBS}

#LDFLAGS = -g ${LIBS}
LDFLAGS = -s ${LIBS}

# compiler and linker
CC ?= cc
