#include "kc8as.h"

#include <stdlib.h>
#include <unistd.h>

int main(int argc, char** argv)
{
    /* Options handling */
    int opt;

    while ((opt = getopt(argc, argv, "vh")) != -1)
    {
        switch (opt)
        {
        case 'v':
            EXIT_VERSION(EXIT_SUCCESS, stdout);
        case 'h':
            EXIT_USAGE(EXIT_SUCCESS, stderr, argv[0]);
        default:
            EXIT_USAGE(EXIT_FAILURE, stderr, argv[0]);
        }
    }

    if (optind >= argc)
        EXIT_USAGE(EXIT_FAILURE, stderr, argv[0]);

    return EXIT_SUCCESS;
}
