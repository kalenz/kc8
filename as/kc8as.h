#ifndef KC8VM_H_
# define KC8VM_H_

# include <stdint.h>
# include <stdio.h>

/* Usage */
#define EXIT_USAGE(exit_status, out, progname) do { \
    fprintf(out, \
        "Usage: %s [-v] [-h] rom\n" \
        " -v        Show version\n" \
        " -h        Show this help\n", progname); \
    exit(exit_status); \
} while (0)

/* Version */
#define EXIT_VERSION(exit_status, out) do { \
    fprintf(out, \
        "kc8as v"VERSION" - A simple chip 8 assembler\n" \
        "Copyright (c) 2012 Nicolas Hureau\n"); \
    exit(exit_status); \
} while (0)

/* Memory */
#define MEM_LOW         0x200
#define MEM_HIGH        0xFFF // 4k ought to be enough
#define MEM_SIZE        MEM_HIGH
#define STACK_SIZE      16

/* Display */
#define DISPLAY_HEIGHT  32
#define DISPLAY_WIDTH   64
#define SPRITE_HEIGHT   8

/* CPU */
#define CPU_FREQ        60

struct chip8_regs
{
    /* General purpose registers
       vf is used for the carry flag */
    uint8_t v[16];

    /* Address register */
    uint16_t i;

    /* Program counter */
    uint16_t pc;

    /* Stack pointer */
    uint16_t sp;

    /* Delay timer register */
    uint8_t dt;

    /* Sound timer register */
    uint8_t st;
};

struct chip8
{
    /* Registers */
    struct chip8_regs regs;

    /* Memory = 4KB */
    uint8_t mem[MEM_SIZE];

    /* Stack allows for 16 recursions, each being 2B => 32B */
    uint16_t stack[STACK_SIZE];

    /* Video buffer */
    uint8_t video[DISPLAY_HEIGHT * DISPLAY_WIDTH];

    /* Map of keys pressed */
    uint8_t key[16];
};

static const uint8_t chip8_fontset[] =
{
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

#endif /* KC8VM_H_ */
