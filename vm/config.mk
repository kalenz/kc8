# kc8vm version
VERSION = 1.0

# Customize below to fit your system

# paths
PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

# SDL
SDLINC = $(shell sdl-config --cflags)
SDLLIB = $(shell sdl-config --libs)

# includes and libs
INCS = -I. -I/usr/include ${SDLINC}
LIBS = -L/usr/lib -lc ${SDLLIB}

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\" -D_GNU_SOURCE
#CFLAGS = -std=c99 -Wall -Wextra -O0 -g ${INCS} ${CPPFLAGS}
CFLAGS = -std=c99 -Wall -Wextra -Os ${INCS} ${CPPFLAGS}
# Solaris
#CFLAGS = -fast ${INCS} -DVERSION=\"${VERSION}\"
#LDFLAGS = ${LIBS}

#LDFLAGS = -g ${LIBS}
LDFLAGS = -s ${LIBS}

# compiler and linker
CC ?= cc
