#include "kc8vm.h"

#include <err.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <SDL/SDL.h>

/* VM */
static void vm_load(struct chip8*, char*);
static int vm_run(struct chip8*);
static void vm_dump(struct chip8*);

/* Display */
static void display_init(void);
static void display_close(void);
static void display_show(struct chip8*);
static uint32_t display_ticks(void);
static void display_delay(uint32_t);

static SDL_Surface* main_surface;

static struct
{
    bool timer;
    bool debug;
} opt = {
    .timer = false,
    .debug = false,
};

int main(int argc, char** argv)
{
    /* Options handling */
    int option;

    while ((option = getopt(argc, argv, "tdvh")) != -1)
    {
        switch (option)
        {
        case 't':
            opt.timer = true;
            break;
        case 'd':
            opt.debug = true;
            break;
        case 'v':
            EXIT_VERSION(EXIT_SUCCESS, stdout);
        case 'h':
            EXIT_USAGE(EXIT_SUCCESS, stderr, argv[0]);
        default:
            EXIT_USAGE(EXIT_FAILURE, stderr, argv[0]);
        }
    }

    if (optind >= argc)
        EXIT_USAGE(EXIT_FAILURE, stderr, argv[0]);

    /* Let's emulate */
    struct chip8 chip8;

    vm_load(&chip8, argv[optind]);
    display_init();

    atexit(display_close);

    return vm_run(&chip8);
}

static
void vm_load(struct chip8* chip8, char* filename)
{
    FILE* f;

    /* Reset all memory */
    memset(chip8, 0, sizeof (*chip8));

    /* Get the rom */
    if ((f = fopen(filename, "rb")) == NULL)
        err(1, "%s", filename);

    fread(chip8->mem + MEM_LOW, sizeof (*chip8->mem), MEM_HIGH - MEM_LOW, f);

    if (ferror(f) || fclose(f) == EOF)
        err(1, "%s", filename);

    /* Start execution at MEM_LOW (0x200) */
    chip8->regs.pc = MEM_LOW;

    /* Copy the fontset in the memory */
    memcpy(chip8->mem, chip8_fontset, sizeof (chip8_fontset));

    srand(time(NULL));
}

static
int vm_run(struct chip8* chip8)
{
    uint16_t opcode;
    uint8_t Vx, Vy;
    int display_changed = 0;

    uint32_t last_time = display_ticks();
    uint32_t diff_time;

    for (;;)
    {
        /* We need 60fps */
        if (opt.timer && (diff_time = display_ticks() - last_time)
                < TIME_PER_FRAME)
            display_delay(TIME_PER_FRAME - diff_time);
        last_time = display_ticks();

        if (chip8->regs.pc > MEM_HIGH)
            errx(2, "IT'S OVER 9000!");

        if (opt.debug)
            vm_dump(chip8);

        /* Fetch the opcode */
        opcode = chip8->mem[chip8->regs.pc] << 8 |
                 chip8->mem[chip8->regs.pc + 1];

        /* Delay timer and sound timer must be updated at 60Hz */
        if (chip8->regs.dt != 0)
            chip8->regs.dt--;
        if (chip8->regs.st != 0)
            chip8->regs.st--;

        switch (opcode & 0xF000)
        {
        case 0x0000:
            switch (opcode)
            {
            /* CLS: Clear the display */
            case 0x00E0:
                DEBUG("CLS");

                memset(chip8->video, 0, DISPLAY_HEIGHT *
                        DISPLAY_WIDTH * sizeof (*chip8->video));
            break;

            /* RET: Return from a subroutine */
            case 0x00EE:
                DEBUG("RET");

                --chip8->regs.sp;
                chip8->regs.pc = chip8->stack[chip8->regs.sp];

                /* Must not increment pc */
                chip8->regs.pc -= 2;
            break;

            /* SYS nnn: Jump to a machine code routine at nnn - obsolete */
            default:
                DEBUG("SYS 0x%.4x", opcode & 0xFFF);

                chip8->regs.pc = opcode & 0xFFF;

                /* Must not increment pc */
                chip8->regs.pc -= 2;
            break;
            }
        break;

        /* JP nnn: Jump to location nnn */
        case 0x1000:
            DEBUG("JP 0x%.4x", opcode & 0xFFF);

            chip8->regs.pc = opcode & 0xFFF;

            /* Must not increment pc */
            chip8->regs.pc -= 2;
        break;

        /* CALL nnn: Call subroutine at nnn */
        case 0x2000:
            DEBUG("CALL 0x%.4x", opcode & 0xFFF);

            chip8->stack[chip8->regs.sp] = chip8->regs.pc + 2;
            ++chip8->regs.sp;
            chip8->regs.pc = opcode & 0xFFF;

            /* Must not increment pc */
            chip8->regs.pc -= 2;
        break;

        /* SE Vx, kk: Skip next instruction if Vx = kk */
        case 0x3000:
            DEBUG("SE V%.1X, 0x%.2x", (opcode & 0xF00) >> 8, opcode & 0xFF);

            if (chip8->regs.v[(opcode & 0xF00) >> 8] == (opcode & 0xFF))
                chip8->regs.pc += 2;
        break;

        /* SNE Vx, kk: Skip next instruction if Vx != kk */
        case 0x4000:
            DEBUG("SNE V%.1X, 0x%.2x", (opcode & 0xF00) >> 8, opcode & 0xFF);

            if (chip8->regs.v[(opcode & 0xF00) >> 8] != (opcode & 0xFF))
                chip8->regs.pc += 2;
        break;

        /* SE Vx, Vy: Skip next instruction if Vx == Vy */
        case 0x5000:
            DEBUG("SE V%.1X, V%.1X",
                    (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

            if (chip8->regs.v[(opcode & 0xF00) >> 8] ==
                    chip8->regs.v[(opcode & 0xF0) >> 4])
                chip8->regs.pc += 2;
        break;

        /* LD Vx, kk: Set Vx = kk */
        case 0x6000:
            DEBUG("LD V%.1X, 0x%.2x", (opcode & 0xF00) >> 8, opcode & 0xFF);

            chip8->regs.v[(opcode & 0xF00) >> 8] = opcode & 0xFF;
        break;

        /* ADD Vx, kk: Set Vx = Vx + kk */
        case 0x7000:
            DEBUG("ADD V%.1X, 0x%.2x", (opcode & 0xF00) >> 8, opcode & 0xFF);

            chip8->regs.v[(opcode & 0xF00) >> 8] += opcode & 0xFF;
        break;

        case 0x8000:
            switch (opcode & 0xF00F)
            {
            /* LD Vx, Vy: Set Vx = Vy */
            case 0x8000:
                DEBUG("LD V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                chip8->regs.v[(opcode & 0xF00) >> 8] =
                    chip8->regs.v[(opcode & 0xF0) >> 4];
            break;

            /* OR Vx, Vy: Set Vx = Vx | Vy */
            case 0x8001:
                DEBUG("OR V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                chip8->regs.v[(opcode & 0xF00) >> 8] |=
                    chip8->regs.v[(opcode & 0xF0) >> 4];
            break;

            /* AND Vx, Vy: Set Vx = Vx & Vy */
            case 0x8002:
                DEBUG("AND V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                chip8->regs.v[(opcode & 0xF00) >> 8] &=
                    chip8->regs.v[(opcode & 0xF0) >> 4];
            break;

            /* XOR Vx, Vy: Set Vx = Vx ^ Vy */
            case 0x8003:
                DEBUG("XOR V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                chip8->regs.v[(opcode & 0xF00) >> 8] ^=
                    chip8->regs.v[(opcode & 0xF0) >> 4];
            break;

            /* ADD Vx, Vy: Set Vx = Vx + Vy */
            case 0x8004:
            {
                DEBUG("ADD V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                uint32_t res = chip8->regs.v[(opcode & 0xF00) >> 8] +
                    chip8->regs.v[(opcode & 0xF0) >> 4];

                /* Vx + Vy > 255 -> VF = 1, carry flag */
                if (res > 255)
                    chip8->regs.v[0xF] = 1;

                chip8->regs.v[(opcode & 0xF00) >> 8] = res & 0xFF;
            }
            break;

            /* SUB Vx, Vy: Set Vx = Vx - Vy */
            case 0x8005:
                DEBUG("SUB V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                Vx = chip8->regs.v[(opcode & 0xF00) >> 8];
                Vy = chip8->regs.v[(opcode & 0xF0) >> 4];

                /* carry flag */
                if (Vx > Vy)
                    chip8->regs.v[0xF] = 1;

                chip8->regs.v[(opcode & 0xF00) >> 8] -= Vy;
            break;

            /* SHR Vx[, Vy]: Set Vx = Vx >> 1 */
            case 0x8006:
                DEBUG("SHR V%.1X[, V%.1X]",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                Vx = chip8->regs.v[(opcode & 0xF00) >> 8];

                /* carry flag */
                if (Vx & 0x1)
                    chip8->regs.v[0xF] = (Vx & 0x1) ? 1 : 0;

                chip8->regs.v[(opcode & 0xF00) >> 8] >>= 1;
            break;

            /* SUBN Vx, Vy: Set Vx = Vy - Vx */
            case 0x8007:
                DEBUG("SUBN V%.1X, V%.1X",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                Vx = chip8->regs.v[(opcode & 0xF00) >> 8];
                Vy = chip8->regs.v[(opcode & 0xF0) >> 4];

                /* carry flag */
                chip8->regs.v[0xF] = Vy > Vx ? 1 : 0;

                chip8->regs.v[(opcode & 0xF00) >> 8] = Vy - Vx;
            break;

            /* SHL [Vx, Vy]: Set Vx = Vx << 1 */
            case 0x800E:
                DEBUG("SHL V%.1X[, V%.1X]",
                        (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

                Vx = chip8->regs.v[(opcode & 0xF00) >> 8];

                /* carry flag */
                chip8->regs.v[0xF] = (Vx & 0x80) ? 1 : 0;

                chip8->regs.v[(opcode & 0xF00) >> 8] <<= 1;
            break;

            default:
                errx(2, "Unknown opcode: %x", opcode);
            break;
            }
        break;

        /* SNE Vx, Vy: Skip next instruction if Vx != Vy */
        case 0x9000:
            DEBUG("SNE V%.1X[, V%.1X]",
                    (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4);

            if (chip8->regs.v[(opcode & 0xF00) >> 8] !=
                    chip8->regs.v[(opcode & 0xF0) >> 4])
                chip8->regs.pc += 2;
        break;

        /* LD I, nnn: Set I = nnn */
        case 0xA000:
            DEBUG("LD I, 0x%.4x", opcode & 0xFFF);

            chip8->regs.i = opcode & 0xFFF;
        break;

        /* JP V0, nnn: Jump to location nnn + V0 */
        case 0xB000:
            DEBUG("JP V0, 0x%.4x", opcode & 0xFFF);

            chip8->regs.pc = chip8->regs.v[0] + (opcode & 0xFFF);

            /* Must not increment pc */
            chip8->regs.pc -= 2;
        break;

        /* RND Vx, kk: Set Vx = random byte & kk */
        case 0xC000:
            DEBUG("RND V%.1X, 0x%.2x", (opcode & 0xF00) >> 8, opcode & 0xFF);

            chip8->regs.v[(opcode & 0xF00) >> 8] =
                (rand() % 256) & (opcode & 0xFF);
        break;

        /* DRW Vx, Vy, n: Display n-bytes sprite starting at memory location I
                          at (Vx, Vy), set VF = collision */
        case 0xD000:
        {
            DEBUG("DRW V%.1X, V%.1X, 0x%.1x",
                    (opcode & 0xF00) >> 8, (opcode & 0xF0) >> 4, opcode & 0xF);

            Vx = chip8->regs.v[(opcode & 0xF00) >> 8];
            Vy = chip8->regs.v[(opcode & 0xF0) >> 4];
            uint8_t height = opcode & 0xF;
            uint8_t pixel;

            chip8->regs.v[0xF] = 0;

            for (unsigned y = 0; y < height; ++y)
            {
                pixel = chip8->mem[chip8->regs.i + y];

                for (unsigned x = 0; x < SPRITE_HEIGHT; ++x)
                {
                    if ((pixel & (0x80 >> x)) != 0)
                    {
                        if (chip8->video[(Vx + x + ((Vy + y) * 64))] == 1)
                            chip8->regs.v[0xF] = 1;

                        chip8->video[Vx + x + ((Vy + y) * 64)] ^= 1;
                    }
                }
            }

            display_changed = 1;
        }
        break;

        case 0xE000:
            switch (opcode & 0xF0FF)
            {
            /* SKP Vx: Skip next instruction if key with the value of Vx is
                       pressed */
            case 0xE09E:
                DEBUG("SKP V%.1X", (opcode & 0xF00) >> 8);

                if (chip8->key[chip8->regs.v[(opcode & 0xF00) >> 8]] != 0)
                    chip8->regs.pc += 2;
            break;

            /* SKNP Vx: Skip next instruction if key with the value of Vx is
                        not pressed */
            case 0xE0A1:
                DEBUG("SKNP V%.1X", (opcode & 0xF00) >> 8);

                if (chip8->key[chip8->regs.v[(opcode & 0xF00) >> 8]] == 0)
                    chip8->regs.pc += 2;
            break;

            default:
                errx(2, "Unknown opcode: %x", opcode);
            break;
            }
        break;

        case 0xF000:
            switch (opcode & 0xF0FF)
            {
            /* LD Vx, DT: Set Vx = delay timer value */
            case 0xF007:
                DEBUG("LD V%.1X, DT", (opcode & 0xF00) >> 8);

                chip8->regs.v[(opcode & 0xF00) >> 8] = chip8->regs.dt;
            break;

            /* LD Vx, K: Wait for a key press, store the value of the key in
                         Vx */
            case 0xF00A:
            {
                DEBUG("LD V%.1X, K", (opcode & 0xF00) >> 8);

                int key_pressed = 0;

                for (unsigned i = 0; i < 16; ++i)
                {
                    if (chip8->key[i] != 0)
                    {
                        chip8->regs.v[(opcode & 0xF00) >> 8] = i;
                        key_pressed = 1;
                    }
                }
            }
            break;

            /* LD DT, Vx: Set delay timer = Vx */
            case 0xF015:
                DEBUG("LD DT, V%.1X", (opcode & 0xF00) >> 8);

                chip8->regs.dt = chip8->regs.v[(opcode & 0xF00) >> 8];
            break;

            /* LD ST, Vx: Set sound timer = Vx */
            case 0xF018:
                DEBUG("LD ST, V%.1X", (opcode & 0xF00) >> 8);

                chip8->regs.st = chip8->regs.v[(opcode & 0xF00) >> 8];
            break;

            /* ADD I, Vx: Set I = I + Vx */
            case 0xF01E:
                DEBUG("ADD I, V%.1X", (opcode & 0xF00) >> 8);

                chip8->regs.i += chip8->regs.v[(opcode & 0xF00) >> 8];
            break;

            /* LD F, Vx: Set I = location of sprite for digit Vx */
            case 0xF029:
                DEBUG("LD F, V%.1X", (opcode & 0xF00) >> 8);

                chip8->regs.i = chip8->regs.v[((opcode & 0xF00) >> 8)] * 5;
            break;

            /* LD B, Vx: Store BCD representation of Vx in memory locations I,
                         I + 1 and I + 2 */
            case 0xF033:
            {
                DEBUG("LD B, V%.1X", (opcode & 0xF00) >> 8);
                unsigned left = chip8->regs.v[(opcode & 0xF00) >> 8];
                for (int i = 2; i >= 0; --i)
                {
                    chip8->mem[chip8->regs.i + i] = left % 10;
                    left /= 10;
                }
            }
            break;

            /* LD [I], Vx: Store registers V0 through Vx in memory starting at
                           location I */
            case 0xF055:
            {
                DEBUG("LD [I], V%.1X", (opcode & 0xF00) >> 8);

                int Vx = (opcode & 0xF00) >> 8;

                for (int i = 0; i < Vx; ++i)
                    chip8->mem[chip8->regs.i + i] = chip8->regs.v[i];
            }
            break;

            /* LD Vx, [I]: Read registers V0 through Vx from memory starting at
                           location I */
            case 0xF065:
            {
                DEBUG("LD V%.1X, [I]", (opcode & 0xF00) >> 8);

                int Vx = (opcode & 0xF00) >> 8;

                for (int i = 0; i < Vx; ++i)
                    chip8->regs.v[i] = chip8->mem[chip8->regs.i + i];
            }
            break;

            default:
                errx(2, "Unknown opcode: %x", opcode);
            break;
            }
        break;

        default:
            errx(2, "Unknown opcode: %x", opcode);
        break;
        }

        if (display_changed)
        {
            display_show(chip8);
            display_changed = 0;
        }

        chip8->regs.pc += 2;
    }
}

static
void vm_dump(struct chip8* chip8)
{
    static int cycle = 0;

    fprintf(stderr, "\n+++ CYCLE %i +++\n", ++cycle);

    /* General purpose registers */
    for (int i = 0; i < 4; ++i)
    {
        for (int j = 0; j < 4; ++j)
            fprintf(stderr, "V%.1X: 0x%.2x   ",
                    4 * i + j, chip8->regs.v[4 * i + j]);
        fprintf(stderr, "\n");
    }

    /* Specific registers */
    fprintf(stderr, "%s: 0x%.2x   ", "DT", chip8->regs.dt);
    fprintf(stderr, "%s: 0x%.2x\n", "ST", chip8->regs.st);
    fprintf(stderr, "%s:  0x%.4x ", "I", chip8->regs.i);
    fprintf(stderr, "%s: 0x%.4x ", "PC", chip8->regs.pc);
    fprintf(stderr, "%s: 0x%.4x\n", "SP", chip8->regs.sp);
}

static
void display_init(void)
{
    if (!opt.debug)
    {
        if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_AUDIO | SDL_INIT_VIDEO) == -1 ||
                (main_surface = SDL_SetVideoMode(DISPLAY_WIDTH * SQUARE_SIZE,
                DISPLAY_HEIGHT * SQUARE_SIZE, 16,
                SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL)
            errx(1, "Could not init SDL display: %s", SDL_GetError());
    }
}

static
void display_close(void)
{
    if (!opt.debug)
        SDL_Quit();
}

static
void display_show(struct chip8* chip8)
{
    SDL_Rect rect;
    uint8_t color;

    static int frame_nbr = 0;

    DEBUG("+++ FRAME %i +++", ++frame_nbr);

    for (unsigned i = 0; i < DISPLAY_HEIGHT; ++i)
    {
        for (unsigned j = 0; j < DISPLAY_WIDTH; ++j)
        {
            color = chip8->video[i * DISPLAY_WIDTH + j] == 1 ? 255 : 0;

            if (opt.debug)
                printf("%c", color == 255 ? '#' : ' ');
            else
            {
                rect.x = j * SQUARE_SIZE;
                rect.y = i * SQUARE_SIZE;
                rect.w = SQUARE_SIZE;
                rect.h = SQUARE_SIZE;

                SDL_FillRect(main_surface, &rect,
                        SDL_MapRGB(main_surface->format, color, color, color));
            }
        }

        if (opt.debug)
            printf("\n");
    }

    if (!opt.debug)
        SDL_Flip(main_surface);
}

static
uint32_t display_ticks(void)
{
    return SDL_GetTicks();
}

static
void display_delay(uint32_t ms)
{
    SDL_Delay(ms);
}
